from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.list import OneLineListItem
from icecream import ic
from kivy.core.window import Window

import os
import subprocess
import pyautogui
import platform
from screeninfo import get_monitors

#kv gui
kv = '''
MDGridLayout:
    cols:1
    MDScrollView:
        MDList:
            id:listContainer
'''

#info about current monitor
monitors = get_monitors()
current_screen = None
for monitor in monitors:
    if monitor.x <= 0 <= monitor.x + monitor.width and monitor.y <= 0 <= monitor.y + monitor.height:
        current_screen = monitor
        break

def read_file( _file_name ):
    striped_lines = []
    with open( _file_name, 'r' ) as file:
        for line in  file.readlines():
            striped_lines.append( line.strip() )

    return striped_lines

class Main(MDApp):
    def build(self):
        Window.borderless = True
        return Builder.load_string(kv)

    def on_start(self):
        def myfunc(event,string=None):
            prefix = "start" if platform.system() == "Windows" else "cmd.exe"
            exit(0)
        self.theme_cls.theme_style = "Dark"
        # work dir
        work_dir = (read_file("config"))[0]
        # work with files in dir
        files = os.listdir(work_dir)
        # for each file add a item in the list
        for script in files:
            item = OneLineListItem(text=script)
            item.bind(on_press=lambda event,string=work_dir+script:myfunc(event,string))
            self.root.ids.listContainer.add_widget(item)
        item = OneLineListItem(text="Cancelar")
        item.bind(on_press=lambda status=0:exit(status))
        self.root.ids.listContainer.add_widget(item)

        #position of component based on current mouse position
        x,y = pyautogui.position()
        Window.left = x
        Window.top = y
        # define size of component
        cols = int(len(files) / 15)
        self.root.ids.listContainer.cols = cols
        Window.size= (250*cols,(len(files)*50)/2)
Main().run()
